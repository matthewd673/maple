﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace maple
{
    class Program
    {

        //for loading in the file
        static string path = "";
        static string ext = "";
        static List<string> document = new List<string>();

        //cursor positions
        static int cX;
        static int cY;

        //render offsets (excluding gutter)
        static int offX = 0;
        static int offY = 0;

        //highlight data
        static string highlightPath = "";
        static string[] hiWords;

        //rendering measurements
        static int bottomLine;
        static int gutterSize;

        static void Main(string[] args)
        {
            //clean up
            Console.Clear();
            Console.Title = "maple";

            Console.BufferHeight = Console.WindowHeight;

            //check for and load file
            if(args.Length < 1)
            {
                Console.WriteLine("Must provide file. Press any key to quit.");
                Console.ReadKey();
            }
            else
            {
                //load in existing file
                if(File.Exists(args[0]))
                {
                    document = File.ReadAllLines(args[0]).ToList();
                    if(document.Count == 0) //file was empty
                        document.Add(""); //make it not empty
                }
                else //make a new empty document, the file will be created when it saves
                    document.Add("");
                path = args[0]; //set the file path to the one we were passed

                //load highlight file (if possible) based on extension
                if(path.Contains("."))
                    ext = path.Split(".")[path.Split(".").Length - 1]; //set the ext
                if(File.Exists("highlight/" + ext + ".txt")) //find and set the highlight file
                    highlightPath = "highlight/" + ext + ".txt";

                //apply highlight words if available
                if(highlightPath != "")
                    hiWords = File.ReadAllLines(highlightPath);
                else
                    hiWords = new string[0]; //empty, to avoid errors later

                //begin the input loop
                input();
            }
        }

        static void input()
        {

            //reset cursor position
            cX = 0;
            cY = 0;

            //do an initial render
            render(true);

            while(true) //the input loop
            {
                ConsoleKeyInfo key = Console.ReadKey();

                switch(key.Key)
                {
                    case ConsoleKey.LeftArrow: //move left (if possible)
                        if(cX > 0)
                            cX--;
                        break;
                    case ConsoleKey.RightArrow: //move right (if possible)
                        if(cX < document[cY].Length)
                            cX++;
                        break;
                    case ConsoleKey.UpArrow: //move up (if possible)
                        if(cY > 0 || (cY == 0 && offY > 0)) //we can move if we aren't at the top or if the top is scrolled
                        {
                            bool atEnd = false;
                            if(cX == document[cY].Length) //check if at end of line
                                atEnd = true;
                            if(cY > 0) //we aren't at the top
                                cY--;
                            else
                                offY--;
                            //sort out x position
                            if(cX > document[cY].Length) //don't run over the next line
                                cX = document[cY].Length;
                            if(atEnd) //cling to end of line
                                cX = document[cY].Length;
                        }
                        break;
                    case ConsoleKey.DownArrow: //move down (if possible)
                        if(cY < document.Count - 1)
                        {
                            bool atEnd = false;
                            if(cX == document[cY].Length) //check if at end
                                atEnd = true;
                            cY++;
                            //set x
                            if(cX > document[cY].Length) //don't run over end
                                cX = document[cY].Length;
                            if(atEnd) //cling to end
                                cX = document[cY].Length;
                        }
                        break;
                    case ConsoleKey.Backspace: //try to backspace
                        if(cX == 0 && cY > 0) //we're backing up to the previous line
                        {
                            int formerLength = document[cY - 1].Length;
                            document[cY - 1] += document[cY]; //combine this line with previous
                            document.RemoveAt(cY);
                            cY--;
                            cX = formerLength; //keep the x where it was
                        }
                        else if(cX > 0) //just remove
                        {
                            document[cY] = document[cY].Remove(cX - 1, 1);
                            cX--;
                        }
                        break;
                    case ConsoleKey.Delete: //try to delete
                        if(cX < document[cY].Length) //just delete
                            document[cY] = document[cY].Remove(cX, 1);
                        if(cX == document[cY].Length && cY < document.Count - 1) //combine with next line if at end
                        {
                            document[cY] += document[cY + 1];
                            document.RemoveAt(cY + 1);
                        }
                        break;
                    case ConsoleKey.Enter:
                        string pushedText = ""; //any text pushed over to the new line
                        if(cX < document[cY].Length)
                        {
                            string replaceLine = "";
                            char[] lineChars = document[cY].ToCharArray();

                            //cycle through finding if any text is pushed or if it stays (based on cX)
                            for(int i = 0; i < lineChars.Length; i++)
                            {
                                if(i >= cX)
                                    pushedText += lineChars[i];
                                else
                                    replaceLine += lineChars[i];
                            }

                            document[cY] = replaceLine; //any text not pushed stays

                        }

                        List<string> afterLines = new List<string>(); //lines that go after the current

                        //add any following lines (and remove them temporarily)
                        for(int i = cY + 1; i < document.Count; i++)
                        {
                            afterLines.Add(document[i]);
                            document.RemoveAt(i);
                            i--;
                        }

                        //add a new line with the pushed text
                        document.Add(pushedText);

                        //add back the following lines
                        foreach(string aLine in afterLines)
                            document.Add(aLine);

                        //set cursor position
                        cY++;
                        cX = 0;
                        break;
                    case ConsoleKey.F1: //save (to be replaced with shortcut)
                        File.WriteAllLines(path, document);
                        break;
                    case ConsoleKey.Escape: //exit (to be replaced with shortcut)
                        Environment.Exit(0);
                        break;
                    default: //by default just add to text
                        Console.Write(key.KeyChar);
                        if(cX < document[cY].Length - 1) //insert in middle
                            document[cY] = document[cY].Insert(cX, key.KeyChar.ToString());
                        else //add to end
                            document[cY] += key.KeyChar.ToString();
                        cX++; //increment position
                        break;
                }

                //render any changes
                render(l: cY - 1);

            }
        }

        static void render(bool fullRender = false, int l = 0)
        {
            if(fullRender) //do it all
            {
                //reset everything
                Console.Clear();
                Console.SetCursorPosition(0, 0);

                int maxScanHeight = offY;

                //determine how far we can go (end of doc or end of screen, whatever comes first)
                if(document.Count - offY <= Console.BufferHeight - 1) //bufferheight - 1, since last is infobar
                    maxScanHeight = document.Count - offY;
                else
                    maxScanHeight = Console.BufferHeight - 1 + offY;

                for(int i = offY - 1; i < maxScanHeight; i++)
                    renderLine(i);
            }
            else //just one line
            {
                if(l > 0)
                    renderLine(l - 1);
                renderLine(l);
                if(l < Console.BufferHeight - 1)
                    renderLine(l + 1);
            }
                /*
                string l = document[i];

                //render line number
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write("{0}│ ", (i + offY).ToString().PadLeft(maxGutterLength, ' '));
                Console.ForegroundColor = ConsoleColor.Gray;

                //render words
                string[] words = l.Split(' ');

                for(int k = 0; k < words.Length; k++)
                {
                    string w = words[k];

                    bool highlight = false;

                    //check if its a highlight word
                    if(hiWords.Length > 0)
                    {
                        foreach(string hW in hiWords)
                        {
                            if(w == hW)
                                highlight = true;
                        }
                    }

                    //set color based on highlight
                    if(highlight)
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                    else
                        Console.ForegroundColor = ConsoleColor.Gray;
                    
                    //write word
                    Console.Write(w);

                    //since we split by space, add a space (unless at end)
                    if(k < words.Length - 1)
                        Console.Write(" ");
                }

                Console.Write("\n"); //new line
                */

            //prep for writing info bar
            Console.SetCursorPosition(0, Console.BufferHeight - 1);
            Console.BackgroundColor = ConsoleColor.DarkYellow;
            Console.ForegroundColor = ConsoleColor.Black;

            //add highlight data to info bar if available
            string highlightInfo = "";
            if(highlightPath != "")
                highlightInfo = "[" + highlightPath.Split('.')[0] + "] ";

            //print info bar
            Console.Write("maple {0} {4}({1}, {2}) / {3}", path, cX, cY, document.Count, highlightInfo);
            
            //set things back to normal
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;

            //reset to user's cursor position (+ gutter)
            Console.SetCursorPosition(cX + gutterSize, cY);
        }

        static void renderLine(int lineNumber)
        {
            if(Console.BufferHeight > lineNumber - offY && lineNumber - offY >= 0)
            {
                //set position and clean line
                Console.SetCursorPosition(0, lineNumber - offY);
                Console.Write(new string(' ', Console.WindowWidth));
                Console.SetCursorPosition(0, lineNumber - offY);

                string l = document[lineNumber];

                //calculate gutter size
                int lineCount = document.Count;
                int maxGutterLength = lineCount.ToString().Length;
                gutterSize = maxGutterLength + 2; //from the other characters;

                //render line number
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write("{0}│ ", (lineNumber + 1).ToString().PadLeft(maxGutterLength, ' '));
                Console.ForegroundColor = ConsoleColor.Gray;

                //render words
                string[] words = l.Split(' ');

                for(int k = 0; k < words.Length; k++)
                {
                    string w = words[k];

                    bool highlight = false;

                    //check if its a highlight word
                    if(hiWords.Length > 0)
                    {
                        foreach(string hW in hiWords)
                        {
                            if(w == hW)
                                highlight = true;
                        }
                    }

                    //set color based on highlight
                    if(highlight)
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                    else
                        Console.ForegroundColor = ConsoleColor.Gray;
                    
                    //write word
                    Console.Write(w);

                    //since we split by space, add a space (unless at end)
                    if(k < words.Length - 1)
                        Console.Write(" ");
                }
            }
        }

    }
}
