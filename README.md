#maple🍁

*Tiny text editor, a work in progress*

If your favorite language is missing a highlight file, please add one


**Features**

* Load and save files
* Automatically highlight syntax
* Display line numbers and other info

**Try it out!**

Run ```debug.bat``` or ```dotnet run [filename]```